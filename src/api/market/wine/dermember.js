import { axios } from '@/utils/request'

/**
 * api接口列表
 * 酒业业绩计算
 */
const api = {
  list: '/market.dermember/list',
  setStatus: '/market.dermember/setStatus'
}

export function getList(params){
  return axios({
    url: api.list,
    method: 'get',
    params
  })
}

export function setStatus(params){
  return axios({
    url: api.list,
    method: 'get',
    params
  })
}
