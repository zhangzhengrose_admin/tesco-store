import { axios } from '@/utils/request'

/**
 * api接口列表
 */
const api = {
  detail: '/market.dealer/detail',
  edit: '/market.dealer/edit',
}

/**
 * 列表记录
 */
export function detail (params) {
  return axios({
    url: api.detail,
    method: 'get',
    params
  })
}


/**
 * 编辑记录
 * @param {*} data
 */
export function edit (data) {
  return axios({
    url: api.edit,
    method: 'post',
    data
  })
}


