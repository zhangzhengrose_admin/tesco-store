import { axios } from '@/utils/request'

/**
 * api接口列表
 * 酒业业绩计算
 */
const api = {
  detail: '/market.perform/detail',
  edit: '/market.perform/edit',
  add: '/market.perform/add',
  list: '/market.perform/getList',
  del: '/market.perform/del',
  setStatus: '/market.perform/setStatus'
}

export function setStatus(params){
  return axios({
    url: api.setStatus,
    method: 'post',
    params
  })
}

export function del(params){
  return axios({
    url: api.del,
    method: 'post',
    params
  })
}

export function getList(params){
  return axios({
    url: api.list,
    method: 'post',
    params
  })
}
/**
 * 添加记录
 */
export function add(params){
  return axios({
    url: api.add,
    method: 'post',
    params
  })
}
/**
 * 列表记录
 */
export function detail (params) {
  return axios({
    url: api.detail,
    method: 'get',
    params
  })
}


/**
 * 编辑记录
 * @param {*} data
 */
export function edit (data) {
  return axios({
    url: api.edit,
    method: 'post',
    data
  })
}


