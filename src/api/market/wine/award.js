import { axios } from '@/utils/request'
/**
 * api接口列表
 * 酒业业绩奖励
 */
const api = {
  award: '/market.award/detail'
}

export function getAward(params){
  return axios({
    url: api.award,
    method: 'get',
    params
  })
}